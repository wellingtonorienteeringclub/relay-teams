﻿namespace RelayTeams;

public interface IFileService {
  EntryList ReadPeople();
  EntryList ReadTeams();
  void WriteTeams(EntryList entryList);
  byte[] GetOutfile();
}