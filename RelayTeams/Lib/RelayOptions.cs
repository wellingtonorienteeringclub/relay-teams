﻿namespace RelayTeams;

public class RelayOptions {
  public string Runners { get; set; }
  public string TeamOriginal { get; set; }
  public string TeamOut { get; set; }
  public List<string> SINumbers { get; set; }
  public bool UseOwnChip { get; set; }
  public int Legs { get; set; }
}