﻿namespace RelayTeams;

public class TeamViewModel {
  public RelayTeam Team { get; set; }
  public List<Runner> Runners { get; set; }
  public string Filter { get; set; }
  public string Message { get; set; }
  public bool Saved { get; set; }
  public List<Runner> AssignedRunners { get; set; }
}