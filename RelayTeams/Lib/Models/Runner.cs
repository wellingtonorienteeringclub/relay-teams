﻿namespace RelayTeams;

public class Runner {
  public string FirstName { get; set; }
  public string LastName { get; set; }
  public string Club { get; set; }
  public string Class { get; set; }
  public string SI { get; set; }
  public string TeamId { get; set; }
}