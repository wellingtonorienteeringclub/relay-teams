﻿namespace RelayTeams;

public class RelayTeam {
  public string Name { get; set; }
  public string Id { get; set; }
  public string Class { get; set; }
  public string Club { get; set; }
  public List<RelayTeamRunner> Runners { get; set; }
}