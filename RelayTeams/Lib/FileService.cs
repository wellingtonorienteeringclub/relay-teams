﻿namespace RelayTeams;

public class FileService : IFileService {
  private readonly IWebHostEnvironment _Env;
  private readonly RelayOptions _RelayOptions;

  public FileService(IWebHostEnvironment env, IOptions<RelayOptions> optionsAccessor) {
    _Env = env;
    _RelayOptions = optionsAccessor.Value;
  }

  private string GetPath(string fileName) {
    var folder = Path.Combine(_Env.ContentRootPath, "Files");

    if (!Directory.Exists(folder)) {
      Directory.CreateDirectory(folder);
    }

    return Path.Combine(folder, fileName);
  }

  public EntryList ReadPeople() {
    var path = GetPath(_RelayOptions.Runners);

    if (!File.Exists(path)) {
      throw new ApplicationException($"Please ensure there is an xml file of type EntryList (IOF v3) in {path}.");
    }

    using (FileStream fileStream = new(path, FileMode.Open)) {
      return fileStream.Deserialize<EntryList>();
    }
  }

  public EntryList ReadTeams() {
    var path = GetPath(_RelayOptions.TeamOriginal);

    if (!File.Exists(path)) {
      throw new ApplicationException($"Please ensure there is an xml file of type EntryList (IOF v3) in {path}.");
    }

    string outPath = GetPath(_RelayOptions.TeamOut);

    if (!File.Exists(outPath)) {
      File.Copy(path, outPath);
    }

    using (FileStream fileStream = new(outPath, FileMode.Open)) {
      var entryList = fileStream.Deserialize<EntryList>();
      entryList.TeamEntry = [.. entryList.TeamEntry.OrderBy(p => Convert.ToInt32(p.Id.Value))];
      return entryList;
    }
  }

  public void WriteTeams(EntryList entryList) {
    using (FileStream fileStream = new(GetPath(_RelayOptions.TeamOut), FileMode.Create)) {
      fileStream.Serialize(entryList);
    }
  }

  public byte[] GetOutfile() {
    return File.ReadAllBytes(GetPath(_RelayOptions.TeamOut));
  }
}