﻿namespace RelayTeams;

public interface IRepository {
  List<Runner> GetRunners();
  List<RelayTeam> GetTeams();
  EntryList ReadTeams();
  void WriteTeams(EntryList entryList);
  void UpdateTeam(string id, List<Runner> entries);
  void AssignSI();
  void AssignCompositeSI();
  byte[] GetOutfile();
  List<Runner> GetAssignedRunners();
}