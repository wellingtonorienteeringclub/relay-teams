﻿namespace RelayTeams;

public class Repository : IRepository {

  private readonly IFileService _FileService;
  private readonly RelayOptions _RelayOptions;

  public Repository(IFileService fileService, IOptions<RelayOptions> optionsAccessor) {
    _FileService = fileService;
    _RelayOptions = optionsAccessor.Value;
  }

  public List<RelayTeam> GetTeams() {
    var te = _FileService.ReadTeams().TeamEntry;

    return te.Select(p => new RelayTeam {
      Id = p.Id.Value,
      Name = p.Name,
      Club = p.Organisation.Select(c => c.Name).FirstOrDefault(),
      Class = p.Class.Select(c => c.Name).FirstOrDefault(),
      Runners = p.TeamEntryPerson.Select(r => new RelayTeamRunner {
        FirstName = r.Person.Name.Given,
        LastName = r.Person.Name.Family,
        SI = (r.ControlCard ?? []).Select(c => c.Value).FirstOrDefault()
      }).ToList()
    }).ToList();
  }

  public List<Runner> GetRunners() {
    return [.. _FileService.ReadPeople().PersonEntry.Select(p => new Runner {
      FirstName = p.Person.Name.Given,
      LastName = p.Person.Name.Family,
      Class = p.Class.Select(c => c.Name).FirstOrDefault(),
      Club = p.Organisation.Name,
      SI = p.ControlCard.Select(c => c.Value).FirstOrDefault()
    }).OrderBy(p => p.FirstName).ThenBy(p => p.LastName).ThenBy(p => p.Club)];
  }

  public EntryList ReadTeams() {
    return _FileService.ReadTeams();
  }

  public void WriteTeams(EntryList entryList) {
    _FileService.WriteTeams(entryList);
  }

  public void UpdateTeam(string id, List<Runner> entries) {
    var te = ReadTeams();

    foreach (var team in te.TeamEntry) {
      if (team.Id.Value != id) {
        continue;
      }

      var people = new List<TeamEntryPerson>();

      for (int i = 0; i < entries.Count; i++) {
        people.Add(new TeamEntryPerson {
          Leg = $"{i + 1}",
          ControlCard = [new ControlCard { Value = entries[i].SI }],
          Person = new Person { Name = new PersonName { Family = entries[i].LastName, Given = entries[i].FirstName } }
        });
      }

      team.TeamEntryPerson = [.. people];
    }

    WriteTeams(te);
  }

  public void AssignSI() {
    List<string> usedSI = GetUsedSIs();
    List<string> rentals = GetRentalSI();
    List<string> available = rentals.Where(p => !usedSI.Contains(p)).ToList();

    int availableIndex = 0;

    var teams = ReadTeams();

    foreach (var team in teams.TeamEntry.OrderBy(p => p.Organisation.Select(o => o.Name).FirstOrDefault())) {
      string club = team.Organisation.Select(p => p.Name).FirstOrDefault();

      if (String.IsNullOrWhiteSpace(club) || club == "Composite") {
        continue;
      }

      foreach (var runner in team.TeamEntryPerson) {
        string si = runner.ControlCard.Select(p => p.Value).FirstOrDefault();

        if (String.IsNullOrWhiteSpace(si)) {
          runner.ControlCard = [new ControlCard { Value = available[availableIndex++] }];
        }
      }
    }

    WriteTeams(teams);
  }

  private List<string> GetUsedSIs() {
    return ReadTeams().TeamEntry
      .SelectMany(p => p.TeamEntryPerson)
      .Select(p => p.ControlCard.Select(c => c.Value).FirstOrDefault())
      .Where(p => !String.IsNullOrWhiteSpace(p))
      .ToList();
  }

  private List<string> GetRentalSI() {
    return _RelayOptions.SINumbers;
  }

  public void AssignCompositeSI() {

    List<string> usedSI = GetUsedSIs();
    List<string> rentals = GetRentalSI();
    List<string> available = rentals.Where(p => !usedSI.Contains(p)).ToList();

    int availableIndex = 0;

    var teams = ReadTeams();

    foreach (var team in teams.TeamEntry.OrderBy(p => p.Organisation.Select(o => o.Name).FirstOrDefault())) {
      string club = team.Organisation.Select(p => p.Name).FirstOrDefault();

      if (String.IsNullOrWhiteSpace(club) || club != "Composite") {
        continue;
      }

      foreach (var runner in team.TeamEntryPerson) {
        string firstName = runner?.Person?.Name?.Given;
        string lastName = runner?.Person?.Name?.Family;

        if (String.IsNullOrWhiteSpace(firstName) || String.IsNullOrWhiteSpace(lastName)) {
          continue;
        }

        string si = runner.ControlCard.Select(p => p.Value).FirstOrDefault();

        if (String.IsNullOrWhiteSpace(si)) {
          runner.ControlCard = [new ControlCard { Value = available[availableIndex++] }];
        }
      }
    }

    WriteTeams(teams);
  }

  public byte[] GetOutfile() {
    return _FileService.GetOutfile();
  }

  public List<Runner> GetAssignedRunners() {
    var people = ReadTeams().TeamEntry.SelectMany(p => p.TeamEntryPerson, (p, c) => new { TeamId = p.Id.Value, Runner = c });

    return people
      .Select(p => new Runner {
        FirstName = p?.Runner?.Person?.Name?.Given,
        LastName = p?.Runner?.Person?.Name?.Family,
        Club = p?.Runner?.Organisation?.Name,
        SI = p?.Runner?.ControlCard?.Select(c => c.Value).FirstOrDefault(),
        TeamId = p.TeamId
      }).
      Where(p => !String.IsNullOrWhiteSpace(p.FirstName) && !String.IsNullOrWhiteSpace(p.LastName))
      .ToList();
  }
}