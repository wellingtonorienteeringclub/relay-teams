﻿namespace RelayTeams;

public static class XmlSerializerExtenstion {
  public static T Deserialize<T>(this Stream stream) {
    return ((T)new XmlSerializer(typeof(T)).Deserialize(stream));
  }

  public static void Serialize<T>(this Stream stream, T instance) {
    new XmlSerializer(typeof(T)).Serialize(stream, instance);
  }
}