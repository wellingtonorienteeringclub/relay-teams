﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RelayTeams;

var builder = WebApplication.CreateBuilder(args);

builder.Services
  .Configure<RelayOptions>(builder.Configuration)
  .AddSingleton<IFileService, FileService>()
  .AddSingleton<IRepository, Repository>()
  .AddControllersWithViews();

var app = builder.Build();

app.UseDeveloperExceptionPage();
app.UseStaticFiles();
app.MapControllers();

app.Run();