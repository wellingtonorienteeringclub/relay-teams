﻿using Microsoft.AspNetCore.Mvc;

namespace RelayTeams;

public class DefaultController : Controller {
  private readonly IRepository _RepositoryService;
  private readonly RelayOptions _Options;

  public DefaultController(IRepository fileService, IOptions<RelayOptions> optionsAccessor) {
    _RepositoryService = fileService;
    _Options = optionsAccessor.Value;
  }

  [HttpGet("/")]
  public ViewResult Index() {
    var teams = _RepositoryService.GetTeams();

    string order = Request.Query["order"] + "";

    if (order == "id") {
      teams = [.. teams.OrderBy(p => p.Id)];
    } else if (order == "class") {
      teams = [.. teams.OrderBy(p => p.Class).ThenBy(c => c.Club).ThenBy(p => p.Name)];
    } else if (order == "club") {
      teams = [.. teams.OrderBy(p => p.Club).ThenBy(c => c.Class).ThenBy(c => c.Name)];
    } else if (order == "team") {
      teams = [.. teams.OrderBy(p => p.Name)];
    }

    return View("Index", teams);
  }

  [HttpGet("/team/{id}")]
  public ViewResult Index(string id) {
    var team = _RepositoryService.GetTeams().FirstOrDefault(p => p.Id == id);
    var runners = _RepositoryService.GetRunners();
    var assignedRunners = _RepositoryService.GetAssignedRunners();
    string filter = Request.Query["filter"] + "";

    if (filter == "both") {
      runners = runners.Where(p => p.Club == team.Club && p.Class == team.Class).ToList();
    } else if (filter == "club") {
      runners = runners.Where(p => p.Club == team.Club).ToList();
    } else if (filter == "class") {
      runners = runners.Where(p => p.Class == team.Class).ToList();
    } else {
      filter = "no";
    }

    return View("Team", new TeamViewModel {
      Team = team,
      Runners = runners,
      Filter = filter,
      AssignedRunners = assignedRunners,
      Saved = Request.Query["saved"] == "true",
      Message = Request.Query["message"]
    });
  }

  [HttpPost("/team/save/{id}")]
  public RedirectResult Save(string id) {
    var runners = _RepositoryService.GetRunners().ToDictionary(k => k.FirstName + " " + k.LastName + " " + k.SI);

    var entries = new List<Runner>();

    for (var i = 1; i <= _Options.Legs; i++) {
      entries.Add(GetRunner(runners, i) ?? new Runner { FirstName = "", LastName = "", SI = "" });
    }

    var assignedRunners = _RepositoryService.GetAssignedRunners();

    string message = "";
    string saved = "false";

    foreach (var r in entries) {
      if (!String.IsNullOrWhiteSpace(r.FirstName) && !String.IsNullOrWhiteSpace(r.LastName) && !String.IsNullOrWhiteSpace(r.SI)) {
        var ar = assignedRunners.FirstOrDefault(p => p.FirstName == r.FirstName && p.LastName == r.LastName && p.SI == r.SI);

        if (ar != null) {
          if (ar.TeamId != id) {
            message += $"{r.FirstName} {r.LastName} {r.SI} is already assigned to another team.<br/>";
          }
        } else {
          assignedRunners.Add(r);
        }
      }
    }

    if (String.IsNullOrWhiteSpace(message)) {
      _RepositoryService.UpdateTeam(id, entries);
      saved = "true";
    }

    string filter = Request.Form["filter"];

    return Redirect($"/team/{id}?filter={filter}&saved={saved}&message={message}");
  }

  [HttpGet("/assign")]
  public RedirectResult Assign() {
    _RepositoryService.AssignSI();
    return Redirect("/");
  }

  [HttpGet("/assigncomposite")]
  public RedirectResult AssignComposite() {
    _RepositoryService.AssignCompositeSI();
    return Redirect("/");
  }

  [HttpGet("/download")]
  public FileContentResult Download() {
    return DownloadCore(true);
  }

  [HttpGet("/download2")]
  public FileContentResult DownloadOriginal() {
    return DownloadCore(false);
  }

  private FileContentResult DownloadCore(bool clean) {
    EntryList entryList = _RepositoryService.ReadTeams();

    if (clean) {
      var entries = entryList.TeamEntry.ToList();
      var indexToRemove = new List<int>();

      for (int i = 0; i < entryList.TeamEntry.Length; i++) {
        var entry = entries[i];

        if (!entry.TeamEntryPerson.Any(p => p.Person != null && p.Person.Name != null && !String.IsNullOrWhiteSpace(p.Person.Name.Given))) {
          indexToRemove.Add(i);
        }
      }

      foreach (var i in indexToRemove.OrderByDescending(p => p)) {
        entries.RemoveAt(i);
      }

      entryList.TeamEntry = [.. entries];
    }

    byte[] rawFile;

    using (var stream = new MemoryStream()) {
      stream.Serialize(entryList);
      stream.Position = 0;
      rawFile = stream.ToArray();
    }

    return File(rawFile, "application/pdf", $"teams-{DateTime.Now:yyMMdd:HHmmss}.xml");
  }

  private Runner GetRunner(Dictionary<string, Runner> mapping, int leg) {
    string key = Request.Form[$"r{leg}"];

    if (!mapping.TryGetValue(key, out Runner value)) {
      return null;
    }

    var runner = value;

    if (!_Options.UseOwnChip) {
      runner.SI = (Request.Form[$"s{leg}"] + "").Trim();
    }

    return runner;
  }
}