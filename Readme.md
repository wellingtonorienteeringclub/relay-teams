# Relay team runner asigner


## Usage

Download latest version from the download tab and unzip it…

``https://bitbucket.org/wellingtonorienteeringclub/relay-teams/downloads/``

Install dot net core framework (latest run time is fine):

On page:
``https://www.microsoft.com/net/download/core``

Download:
``https://www.microsoft.com/net/download/thank-you/dotnet-runtime-2.0.7-windows-x64-asp.net-core-runtime-installer``

If the runtime version is not working try the SDK.

## Setup

Copy ``teams.xml`` to ``./files/teams.xml`` this is a IOF 3.0 xml EntryList file with multiple TeamEntry nodes.
Copy ``runners.xml`` to ``./files/runners.xml`` this is a IOF 3.0 xml EntryList file with multiple PersonEntry nodes.

## Running the app
Open powershell (windows key and type powershell) and navigate (cd) to the folder you extracted the zip to:

Type: ``dotnet .\RelayTeams.dll`` to run the application. You should see text in the powershell console saying “Now listning on: http://localhost:5000”

Open a browser (tested in Chrome) and navigate to http://localhost:5000 or whatever it said above.

## Thoughts…

The application updates the ``teams-out.xm`` file in the ``files`` folder. If something get bad and you need to restart just remove ``teams-out.xml`` and everything will be like it was at the start.

The “row” goes green when all names and si has been assigned to a team.

The different edit buttons filters the drop downs for assigning the runners. You should only need the main edit button except for when people is not running their correct grade and composite teams.

If it is a composite team use those instead of assinging runners to a normal team team...

I find it easiest to open all non composite teams for one class in multiple “tabs”. After filling out “Team for school A - 1”, you can refresh the page in the “Team for school A - 2” tab and the drop downs will now remove the already assigned runners.

Do the composite teams last! The edit button for them will show all remaining runners in that grade (so composite gets treated as a school).

You cannot assign the same runner twice he need to be remove from the previous team first by selecting the empty option.

You need to rename teams in OS2010.
Add extra non entered people in OS2010 after you do the final download or add them to the ``runners.xml``

After you done a few teams you can “backup” the “teams-out.xml” file by copying it. If you need to restart from that point, delete “teams.xml” (keep you might need to start from the beginning again) and “teams-out.xml” and rename your copy “teams.xml”

Not sure if you can run this on the tablet but it should most likely work.

There is an appsettings.json file that contains the paths to the file and the rental SIs. 

The download button will give you a file to import into OS2010. There is an option in the entries menu to import runners into teams. When importing select xml and to identify teams by entry id. (Please do a backup before)

There are probably some bugs…